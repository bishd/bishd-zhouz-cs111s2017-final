import java.util.*;

public class FinalDecipher {
	//these variables don't need to be private, but these are pulled from the FinalMain class
	private int offset;
	private char[] array;
	//these construct the variables, giving them a value
	public FinalDecipher(int o, char[] a){
		offset = o;
		array = a;
		decrypt();
	}
	//this is the method which actually decrypts the message, which actually ends up displaying the original method that the user input at the beginning, since it decrypts the encrypted message that the program just made
	private void decrypt() {
		System.out.println("The deciphered message is ");
		for(int i=0; i < array.length; i++){
		array[i] -= offset;
		}
		for(int i=0; i < array.length; i++){
		System.out.print(array[i]); 	
		}
		System.out.println();
		System.out.println("This is with an offset of " + offset);
		System.out.println("This is going to be the original message you typed");

		

	}
	
}
	
