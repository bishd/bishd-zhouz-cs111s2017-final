
//*************************************
// Honor Code: The work I am submitting is a result of my own thinking and efforts.
// Dylan Bish
// CMPSC 111 Spring 2017
// Lab #
// Date: 1 May 2017
//
// Purpose: To finish the semester
//*************************************
import java.util.Date; // needed for printing today's date
import java.util.*;
public class FinalMain //replace xxx with actual file name
{
	//-------------
	//main method: program execution begins
	//-------------
	public static void main(String[] args)
	{
		// this is the command line we implemented to determine the offset of the cipher, based off of the Caeser method of encryption: the user must enter an additonal command line, an integer, to run this program
		//citation note: I got this idea from the TA Zac Shaeffer, I originally intended to use a scan command but Zac suggested this idea instead
		int offset = Integer.parseInt(args[0]);

		//Label output with name and date:
		System.out.println("Dylan Bish & Zijun Zhou\nFinal\n" + new Date() + "/n");
		
		//Variable dictionary:
		System.out.println("Welcome to the ciphering program!\nPlease enter a message you would like to encrypt.\nIf you are decrypting a message, input the ciphered message you received to decrypt it, using the negative counterpart of your integer as the offset\nLastly, if you are typing a message with spaces, please use an underscore or other symbol to as a space instead:");

		
		Scanner scan = new Scanner(System.in);
		//this takes the user input and converts it into an array of characters
		String message = scan.next();
		char[] array = message.toCharArray();
		System.out.println("The message you entered was:");
		// the foor loop is necessary for printing the message, allowing the user to see what they typed
		for(int i=0; i < array.length; i++){
		System.out.print(array[i]); 	
		}
	System.out.println();
		// these constructors are necessary for moving on to the next classes, without them, the program ends here and virtually does nothing but print out the characters of the array
		FinalCipher cipher = new FinalCipher(offset, array);
		FinalDecipher decipher = new FinalDecipher(offset, array);
	}
}

