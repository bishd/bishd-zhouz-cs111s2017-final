import java.util.*;

public class FinalCipher {
	//these variables don't need to be private, but these are pulled from the FinalMain class
	private int offset;
	private char[] array;
	//these construct the variables, giving them a value
	public FinalCipher(int o, char[] a){
		offset = o;
		array = a;
		encrypt();
	}
	//this is the method that encrypts the message, and displays a completely new message.  if the user input an encrypted message, assuming the user also input the correct opposite integer, it will actually decrypt the message, displaying the hidden message
	private void encrypt() {
		System.out.println("The new secret message is ");
		for(int i=0; i < array.length; i++){
		array[i] += offset;
		}
		for(int i=0; i < array.length; i++){
		System.out.print(array[i]); 	
		}
		System.out.println();
		System.out.println("This is with an offset of " + offset);

		

	}
	
}
	
